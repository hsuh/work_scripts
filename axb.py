import numpy as np

#X= np.array([[0.687515,0.165299,0.131109,0.195782,0.599285,0.264607,0.381122,0.321257,0.421507,0.00486455]])
#AX = np.array([[6.47626,8.98733,9.06295,8.0969,10.027,9.01172,9.29097,11.3955,6.97592,7.27505]])
X = np.array([[0.047921,0.0115216,0.00913851,0.0136464,0.0417712,0.0184436,0.0265649,0.0223921,0.0293798,0.000339067]])
AX = np.array([[0.451407,0.626432,0.631704,0.564368,0.698902,0.628132,0.647596,0.794285,0.486234,0.507084]])
b = np.array([[0.974623,0.234328,0.18586,0.277541,0.849549,0.375108,0.54028,0.455414,0.59753,0.00689599]])
#Ax + x
Axpx = AX + X

# 0.5 x^T (AX + X)
temp = 0.5*(X @ Axpx.T)
f = temp*0.5

#0.5 x^T (Ax)
f_orig = 0.5*np.dot(X,AX.T)

# 0.5 x^T(AX) - b^T X
f_orig_two = f_orig - np.dot(b,X.T)

#0.5 x^T (AX) - b^T X +  0.5 \|x\|_2^2
f_total = f_orig_two +  0.5*np.dot(X,X.T)

# 0.5 \|x\|_2^2
reg_obj = 0.5*np.dot(X,X.T)

# 0.5 x^T (Ax) - b^T x
orig_two = f_orig - np.dot(b,X.T)

#reg obj
reg_obj = 0.5*np.dot(X,X.T)

#grad orig
grad_orig = AX - b

#grad: Ax + x - b
grad = Axpx - b

import pdb
pdb.set_trace()
